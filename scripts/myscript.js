$( document ).ready(function() {
	// Получаем данные из http://university.netology.ru/api/currency.
	var jqxhr = $.getJSON( "http://university.netology.ru/api/currency", function(current) {
		// Заполняем теги select с классом currency тегами option и сразу проставляем в них данные.
		current.forEach(function(exchange) {
			$( ".currency" ).append("<option value=\"" + exchange.Nominal / exchange.Value + "\" data-charcode=\"" + exchange.CharCode + "\">" + exchange.Name + "</option>");
		});
	});

	// Алярма, если Нетология !опять не дает ресурсы.
	jqxhr.fail(function() {
		alert( "Данные для конвертера сейчас не доступны." );
	});

	var orig = "originalSumCurrency"; // id input'а левой части формы.
	var resul = "resultingSumCurrency"; // id input'а правой части формы.

	// Основная функция обработки.
	function calculation(id) {
		var difference, output;
		var enteredValue = $( "#" + id ).val(); // Захват значения из активного поля ввода.
		var originalValue = $( "#original option:selected" ).val(); // Захват значения из option.
		var resultingValue = $( "#resulting option:selected" ).val(); // Захват значения из option.
		if (id == orig) {
			difference = resultingValue / originalValue;
			output = resul; // Куда выводить.
		} else {
			difference = originalValue / resultingValue;
			output = orig; // Куда выводить.
		}
		$( "#" + output ).val(Math.round(enteredValue * difference *100) / 100);
	}

	// Событие по взаимодействияю с полями ввода.
	$( "input" ).bind( "keyup change input", function () {
		// Условие ввода символов.
		if (this.value.match(/^\.|\d+\..*\.|[^\d\.{1}]/g)) {
			this.value = this.value.slice(0,-1);
		}
		var inputId = $( this ).attr("id");
		calculation(inputId);
	});

	// Событие по взаимодействияю с полями выбора валют.
	$( "select" ).change(function() {
		var blockClass = $( this ).parent().attr("class"); // Класс родителя селекта.
		if (blockClass == "converterRight") {
			calculation(orig);
		} else {
			calculation(resul);
		}
	});

	// Демонстрация работы конвертера при загрузке страницы.
	jqxhr.always(function() {
		$( "#resulting option[data-charcode=\"EUR\"]" ).prop("selected", true); // Значение левого поля выбора валюты при загрузке страницы.
		$( "#" + orig ).val(1000); // Значение левого поля ввода при загрузке страницы.
		calculation(orig);
	});
});